{ region ? "us-west-2"
, dbUser
, dbPass
, dbName
, dbPort ? 5432
, webPort ? 8080
, twilioAuthId
, twilioAuthToken
, ...
}:

{
  network.description = "remind-me-to-vote";

  remindMeToVote =
    { config, pkgs, resources, ... }:
      let
        modifiedHaskellPackages =  pkgs.haskell.packages.lts-6.override {
          overrides = self: super: {
            multi-source-config = self.callPackage ../../../multi-source-config {};
            remind-me-to-vote = self.callPackage ../. {};
          };
        };
        package = modifiedHaskellPackages.remind-me-to-vote;
      in
        {
          deployment.targetEnv = "ec2";
          deployment.ec2.region = region;
          deployment.ec2.instanceType = "t2.micro";
          deployment.ec2.securityGroups = [ "default" "allow-ssh" "allow-http" ];
          deployment.ec2.keyPair = resources.ec2KeyPairs.remind-me-to-vote-keys;

          networking.hostName = "remind-me-to-vote";
          networking.firewall.allowedTCPPorts = [ 22 80 webPort ];

          # Port forwarding using iptables, could alternatively do
          # http://stackoverflow.com/a/21653102, but no biggie, since we don't
          # intended on bringing the service up and down regularly
          networking.firewall.extraCommands = ''
            iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port ${toString webPort}
          '';

          nix.gc.automatic = true;

          deployment.keys.remind-me-to-vote-environment.text = ''
            PORT=${toString webPort}
            PGUSER=${dbUser}
            PGPASS=${dbPass}
            PGDATABASE=${dbName}
            PGPORT=${toString dbPort}
            TWILIO_AUTH_ID=${twilioAuthId}
            TWILIO_AUTH_TOKEN=${twilioAuthToken}
          '';

          environment.systemPackages = [ package pkgs.postgresql ];

          systemd.services.remind-me-to-vote-server = {
            description = "remind-me-to-vote server";
            wantedBy = [ "multi-user.target" ];
            after = [ "network.target" ];
            # These are set here because the expressions do not resolve
            # correctly inside the deployment.keys.* attribute, the RDS endpoint
            # is empty and the path for assets is wrong
            environment.PGHOST = "${builtins.head (pkgs.lib.splitString '':'' resources.rdsDbInstances.${dbName}.endpoint)}";
            environment.STATIC_DIR = "${package}/assets";

            serviceConfig = {
              ExecStart = "${package}/bin/server";
              Restart = "on-failure";
              User = "remind-me-to-vote";
              EnvironmentFile = "/run/keys/remind-me-to-vote-environment";
            };
          };

          systemd.services.remind-me-to-vote-daemon = {
            description = "remind-me-to-vote daemon";
            wantedBy = [ "multi-user.target" ];
            after = [ "network.target" ];
            # These are set here because the expressions do not resolve
            # correctly inside the deployment.keys.* attribute, the RDS endpoint
            # is empty and the path for assets is wrong
            environment.PGHOST = "${builtins.head (pkgs.lib.splitString '':'' resources.rdsDbInstances.${dbName}.endpoint)}";

            serviceConfig = {
              ExecStart = "${package}/bin/daemon";
              Restart = "on-failure";
              User = "remind-me-to-vote";
              EnvironmentFile = "/run/keys/remind-me-to-vote-environment";
            };
          };

          services.nixosManual.showManual = false;

          services.openssh = {
            allowSFTP = false;
            passwordAuthentication = false;
          };

          users = {
            mutableUsers = false;
            users.root.openssh.authorizedKeys.keyFiles = [ ~/.ssh/id_rsa.pub ];
          };

          users.extraUsers.remind-me-to-vote = {
            name = "remind-me-to-vote";
            description = "remind-me-to-vote user";
          };
        };

  resources.ec2KeyPairs.remind-me-to-vote-keys = { inherit region; };

  resources.rdsDbInstances.${dbName} = {
    inherit region dbName;
    id = builtins.replaceStrings ["_"] [""] dbName;
    instanceClass = "db.t2.micro";
    allocatedStorage = 20;
    masterUsername = dbUser;
    masterPassword = dbPass;
    port = dbPort;
    engine = "postgres";
  };
}
