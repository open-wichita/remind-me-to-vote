# Remind Me To Vote

Simple service to allow folks to enroll for notifications/reminders about
upcoming election-related events.

Built with:

* Haskell (Spock, Persistent, Lucid and more)
* AWS
* Postgres
* Twilio
* NixOS


# Build

Download and install [stack](https://www.haskell.org/downloads#stack). Then in
the project directory run the (mostly) one-time setup:

```
$ stack setup
```

From then on you can just:

```
$ stack build
```


# Run

To run the server you can use `stack exec server` and it will execute the
generated executable for you (and rebuild it if necessary). The default port is
3000, as specified in `config/settings.yml`. You'll also need a PostgreSQL
server running with a database setup for the app. The app will create the
schema, but you need to be sure it has something to talk to.

During development it's convenient to use GHCi and DevelMain.hs instead of
repeatedly starting and stopping the server. Start up GHCi with either `stack
ghci` depending on what you're using. Then, inside of this GHCi session,
execute:

```
> :l DevelMain
> DevelMain.update
```

Run those commands again anytime you want to update the server in-place. When
you're done with the session, close GHCi with `Ctrl-d`.


# Deploy

You'll need [nix](https://nixos.org/nix/), nixops (`nix-env -i nixops`) and
`git-crypt` (`nix-env -i git-crypt`). Run `git-crypt unlock
/path/to/key/you/were/given` once to decrypt the encrypted files needed to
deploy.

You'll need to source in the `.envrc` file whenever you want to run
nixops or have [direnv](https://github.com/direnv/direnv) setup, which will do
it automatically. It sets up environment variables needed for a consistent build
environment and deployment secrets.

Once you have the nix stuff installed and sourced in the environment file, run
`nixops deploy -d remind-me-to-vote`. This will build and deploy the application
to production.

If you need to get onto the production machine, use
`nixops ssh -d remind-me-to-vote remindMeToVote`.


# Alternate Build Setups

You should probably just stick to using Stack, but if you want to do something
different, here are some alternatives. All commands here assume you are in the
root of the project unless otherwise stated.


## Cabal

If you want to go with vanilla tools, download and install a [minimal GHC 7.10.3
environment](https://www.haskell.org/downloads#minimal). You'll also currently
need to clone
[`multi-source-config`](https://gitlab.com/doshitan/multi-source-config) as it
is not on Hackage currently.

After that, run some initialization stuff:

```
$ cabal sandbox init
$ cabal sandbox add-source <path to multi-source-config>
$ cabal install --only-dependencies
```

From then on you can just:

```
$ cabal build
```

## Nix + Cabal

Download and install [nix](https://nixos.org/nix/). You'll probably also
want/need to be on the unstable channel so run:

```
$ nix-channel --add http://nixos.org/channels/nixpkgs-unstable
$ nix-channel --update
```

You'll also currently need to clone
[`multi-source-config`](https://gitlab.com/doshitan/multi-source-config) as it
is not on Hackage/in nixpkgs currently. Update the path in `shell.nix` to
correspond to where you cloned `multi-source-config`. You'll now need to install
`cabal2nix`, which is easiest to do with nix itself, `nix-env -i cabal2nix`. In
the `multi-source-config` checkout you'll need to run:

```
$ cabal2nix . > default.nix
```

This takes the information in the cabal file and generates the file nix needs in
order to build the project(s). If the cabal file ever changes, you have to
regenerate the nix file by running the command again.

From there, configure the remind-me-to-vote project with nix:

```
$ nix-shell --command 'cabal configure'
```

And then you can use cabal as normal:

```
$ cabal build
```

### Aside

You can also just run:

```
$ nix-shell
```

Which drops you into a special shell environment with the all the things needed
to build the project set up. You can run cabal commands and such in here and
just exit when you're done. The downside with this is that this shell is Bash
and [can only be Bash currently](https://github.com/NixOS/nix/pull/545), which
kinda stinks if you normally use a different shell (e.g., ZSH). The previous
approach (`nix-shell --command 'cabal configure'`) just enters this shell and
runs the specified command (as you would expect), cabal saves all the
information/paths from this configure phase which enables you to only have to
configure under `nix-shell` and otherwise run commands in your own shell.
