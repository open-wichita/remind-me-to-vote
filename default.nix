{ mkDerivation, aeson, base, blaze-builder, classy-prelude
, cryptonite, digestive-functors, digestive-functors-lucid
, email-validate, esqueleto, file-embed, foreign-store, http-types
, human-readable-duration, hvect, lucid, memory, monad-logger, mtl
, multi-source-config, persistent, persistent-postgresql
, persistent-template, resourcet, scrypt, Spock, stdenv, text, time
, twilio, unordered-containers, wai, wai-extra
, wai-middleware-static, warp, yaml
}:
let
  cleanSource = name: type: let baseName = baseNameOf (toString name); in ! (
      (type == "directory" && baseName == ".git") ||
      (type == "directory" && baseName == "dist") ||
      baseName == "state.nixops"
    );
in mkDerivation {
  pname = "remind-me-to-vote";
  version = "0.1.0.0";
  src = builtins.filterSource cleanSource ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base blaze-builder classy-prelude cryptonite
    digestive-functors digestive-functors-lucid email-validate
    esqueleto file-embed foreign-store http-types
    human-readable-duration hvect lucid memory monad-logger mtl
    multi-source-config persistent persistent-postgresql
    persistent-template resourcet scrypt Spock text time twilio
    unordered-containers wai wai-extra wai-middleware-static warp yaml
  ];
  executableHaskellDepends = [ base ];
  postInstall = ''
    cp -rp src/assets $out/
  '';
  license = stdenv.lib.licenses.agpl3;
}
