{ pkgs ? (import <nixpkgs> {})
, haskellPackages ? pkgs.haskell.packages.lts-6
, haskellDevTools ? (if pkgs ? myHaskellDevTools then pkgs.myHaskellDevTools else (p : []))
}:

let
  modifiedHaskellPackages = haskellPackages.override {
    overrides = self: super: {
      multi-source-config = self.callPackage ../../multi-source-config {};
      remind-me-to-vote = self.callPackage ./. {};
    };
  };

  package = with modifiedHaskellPackages;
    pkgs.haskell.lib.addBuildTools
      remind-me-to-vote
      (haskellDevTools modifiedHaskellPackages);
in
  package.env
