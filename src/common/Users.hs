{-# LANGUAGE RankNTypes #-}
module Users where

import           Db


import           ClassyPrelude        hiding (hash)
import           Crypto.Scrypt        (EncryptedPass (..), Pass (..),
                                       ScryptParams, defaultParams,
                                       encryptPassIO, verifyPass)
import           Data.Maybe           (fromJust)
import           Database.Persist


scryptParams :: ScryptParams
scryptParams = defaultParams


registerUser :: Text -> Text -> DbM (Either Text Text)
registerUser username password = do
  mUserU <- getBy (UniqueUsername username)
  case mUserU of
    Just _ ->
      return (Left "Username already taken!")
    Nothing -> do
      encryptedPass <- liftIO $ encryptPassIO scryptParams (Pass $ encodeUtf8 password)
      _ <- insert (User username (getEncryptedPass encryptedPass) True Subscriber)
      return (Right "Signup complete. You may now login.")


loginUser :: Text -> Text -> DbM (Maybe (Entity User))
loginUser username password = do
  mUserU <- getBy (UniqueUsername username)
  case mUserU of
    Just userEntity ->
      let user = entityVal userEntity
      in case verifyPass scryptParams (Pass $ encodeUtf8 password) (EncryptedPass $ userPassword user) of
        (True, mUpdatedPass) -> do
          when (isJust mUpdatedPass) $
            update (entityKey userEntity) [UserPassword =. getEncryptedPass (fromJust mUpdatedPass)]
          return $ Just userEntity
        (False, _) -> return Nothing
    Nothing ->
      return Nothing
