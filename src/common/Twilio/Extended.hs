module Twilio.Extended
  ( module Twilio
  , verifyRequest
  ) where

import           ClassyPrelude
import           Crypto.Hash.Algorithms   (SHA1)
import           Crypto.MAC.HMAC          (HMAC (hmacGetDigest), hmac)
import           Data.ByteArray.Encoding  (Base (Base64), convertToBase)
import           Twilio


-- | Verify a request is coming from Twilio
-- https://www.twilio.com/docs/api/security#validating-requests
verifyRequest :: Text -- ^ URI of requested enpoint
              -> [(Text, Text)] -- ^ The request parameters
              -> Text -- ^ Signature provided in request header
              -> AuthToken -- ^ Twilio auth token
              -> Bool
verifyRequest uri reqParams sig authToken =
  b64Hash == encodeUtf8 sig
  where
    pars = concatMap (uncurry (<>)) (sortOn fst reqParams)
    msg = uri <> pars
    hash' = hmac (encodeUtf8 . getAuthToken $ authToken) (encodeUtf8 msg) :: HMAC SHA1
    b64Hash = convertToBase Base64 $ hmacGetDigest hash'
