{-# LANGUAGE CPP             #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
-- | Settings are centralized, as much as possible, into this file. This
-- includes database connection settings, static file locations, etc.
module Settings where

import           ClassyPrelude
import           Control.Exception           (throw)
import           Data.Aeson                  (FromJSON (..), Result (..), Value,
                                              fromJSON, withObject, (.:))
import           Data.FileEmbed              (embedFile)
import           Data.Yaml                   (decodeEither')
import           Database.Persist.Postgresql (PostgresConf)
import           MultiSourceConfig           (applyEnvValue, configSettingsYml)
import           Network.Wai.Handler.Warp    (HostPreference)
import           Twilio.Types                (AccountSID, AuthToken)


-- | Runtime settings to configure this application. These settings can be
-- loaded from various sources: defaults, environment variables, config files,
-- theoretically even a database.
data AppCfg = AppCfg
  { appDatabaseCfg :: PostgresConf
  -- ^ Configuration settings for accessing the PostgreSQL database.
  , appStaticDir   :: Text
  -- ^ Directory from which to serve static files.
  , appHost        :: HostPreference
  -- ^ Host/interface the server should bind to.
  , appPort        :: Int
  -- ^ Port to listen on
  , appTwilioCfg   :: TwilioCfg
  }


instance FromJSON AppCfg where
  parseJSON = withObject "AppCfg" $ \o -> do
    appDatabaseCfg <- o .: "database"
    appStaticDir   <- o .: "static-dir"
    appHost        <- fromString <$> o .: "host"
    appPort        <- o .: "port"
    appTwilioCfg   <- o .: "twilio"

    return AppCfg {..}


data TwilioCfg = TwilioCfg
  { twilioAuthId    :: AccountSID
  , twilioAuthToken :: AuthToken
  , twilioNumber    :: Text
  }


instance FromJSON TwilioCfg where
  parseJSON = withObject "TwilioCfg" $ \o -> do
    twilioAuthId    <- o .: "auth-id"
    twilioAuthToken <- o .: "auth-token"
    twilioNumber    <- o .: "number"

    return TwilioCfg {..}


-- | Raw bytes at compile time of @config/settings.yml@
configSettingsYmlBS :: ByteString
configSettingsYmlBS = $(embedFile configSettingsYml)


-- | @config/settings.yml@, parsed to a @Value@.
configSettingsYmlValue :: Value
configSettingsYmlValue = either throw id $ decodeEither' configSettingsYmlBS


-- | A version of @AppCfg@ parsed at compile time from @config/settings.yml@.
compileTimeAppCfg :: AppCfg
compileTimeAppCfg =
  case fromJSON $ applyEnvValue False mempty configSettingsYmlValue of
    Error e -> error e
    Success settings -> settings
