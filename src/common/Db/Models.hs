{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
module Db.Models where

import           Db.Types


import           ClassyPrelude
import           Database.Persist.TH


share [mkPersist sqlSettings, mkDeleteCascade sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Subscription
  active Bool
  deriving Show
SubscriptionPhoneNumber
  subscription SubscriptionId
  number Text
  UniqueNumber number
  deriving Show
SubscriptionEmailAddress
  subscription SubscriptionId
  address Text
  UniqueEmail address
  deriving Show
SubscriptionReminderSent
  subscription SubscriptionId
  reminder ReminderId
  messageSid MessageSID
  Primary subscription reminder
  deriving Show
Preferences
  subscription SubscriptionId
  localElections Bool
  stateElections Bool
  federalElections Bool
  registrationDeadlines Bool
  deriving Show
Event
  name Text
  occurs UTCTime
  deriving Show
Reminder
  occurs UTCTime
  message Text
  sent Bool
  event EventId Maybe
  deriving Show
User
  name Text
  password ByteString
  active Bool
  role Role
  UniqueUsername name
  deriving Show
|]
