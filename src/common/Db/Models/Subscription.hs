module Db.Models.Subscription where

import           ClassyPrelude
import qualified Database.Esqueleto as E
import           Database.Persist   (Entity (..))
import           Db.Models
import           Db.Types


getSubscriptionsQuery :: DbM [(Entity Subscription, Entity SubscriptionPhoneNumber)]
getSubscriptionsQuery =
  E.select $
    E.from $ \(s, sn) -> do
      E.where_ (s E.^. SubscriptionId E.==. sn E.^. SubscriptionPhoneNumberSubscription)
      E.orderBy [E.asc (s E.^. SubscriptionId)]
      return (s, sn)



