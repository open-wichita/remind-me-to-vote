{-# LANGUAGE KindSignatures  #-}
{-# LANGUAGE RankNTypes      #-}
{-# LANGUAGE TemplateHaskell #-}
module Db.Types
  ( DbM
  , Role (..)
  , PreferenceOption (..)
  , MessageSID
  ) where


import           ClassyPrelude
import           Database.Persist.Sql (SqlBackend)
import           Database.Persist.TH
import           Twilio.Types.SID    (MessageSID)


-- See https://github.com/yesodweb/yesod-scaffold/issues/69
type DbM a = forall (m :: * -> *).
    (MonadIO m, Functor m) => ReaderT SqlBackend m a


data Role
  = Admin
  | Subscriber
  deriving (Enum, Eq, Read, Show)


derivePersistField "Role"


data PreferenceOption
  = LocalElections
  | StateElections
  | FederalElections
  | RegistrationDeadlines
  deriving (Enum, Eq, Read, Show)


derivePersistField "PreferenceOption"

derivePersistField "MessageSID"
