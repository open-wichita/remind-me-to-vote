{-# LANGUAGE FlexibleContexts #-}
module Main where


import           Db
import           Db.Models.Subscription      (getSubscriptionsQuery)
import           Settings                    (AppCfg (..), TwilioCfg (..),
                                              configSettingsYmlValue)


import           Control.Concurrent          (threadDelay)
import           Control.Monad               (forM_, forever, unless, void)
import           Control.Monad.Logger        (runStdoutLoggingT)
import           Data.Maybe                  (isJust)
import           Data.Time.Clock             (getCurrentTime)
import           Database.Persist            (Entity (..), SelectOpt (Asc),
                                              insert, selectFirst, selectList,
                                              update, (<=.), (=.), (==.))
import           Database.Persist.Postgresql (createPostgresqlPool, pgConnStr,
                                              pgPoolSize, runSqlPool)
import           MultiSourceConfig           (configSettingsYml,
                                              loadAppSettings,
                                              loadAppSettingsArgs, useEnv)
import           Prelude
import           Twilio.Extended             (runTwilio)
import           Twilio.Message              (Message (sid))
import           Twilio.Messages             (PostMessage (..), post)


main :: IO ()
main = do
  -- Get the settings from all relevant sources
  appCfg <- loadAppSettingsArgs
    -- fall back to compile-time values, set to [] to require values at runtime
    [configSettingsYmlValue]

    -- allow environment variables to override
    useEnv

  pool <- runStdoutLoggingT $ createPostgresqlPool
    (pgConnStr  $ appDatabaseCfg appCfg)
    (pgPoolSize $ appDatabaseCfg appCfg)

  let twilioCfg = appTwilioCfg appCfg
      runTwilio' = runTwilio (twilioAuthId twilioCfg, twilioAuthToken twilioCfg)
      runSql action = runStdoutLoggingT $ runSqlPool action pool

  forever $ do
    now <- getCurrentTime

    -- get the reminders that need sent
    reminders <- runSql $ selectList [ReminderOccurs <=. now, ReminderSent ==. False] [Asc ReminderOccurs]

    -- figure out where we are sending each reminder, which for now, is just everyone
    subs <- runSql getSubscriptionsQuery

    -- send each reminder to each subscriber, naively at the moment
    forM_ reminders $ \(Entity reminderId reminder) -> do
      forM_ subs $ \(Entity subId _, Entity _ subNumber) -> do
        mSent <- runSql $ selectFirst
          [ SubscriptionReminderSentSubscription ==. subId
          , SubscriptionReminderSentReminder ==. reminderId
          ] []
        unless (isJust mSent) $ do
          msg <- runTwilio' $ post PostMessage
            { sendFrom = twilioNumber twilioCfg
            , sendTo = subscriptionPhoneNumberNumber subNumber
            , sendBody = reminderMessage reminder
            }
          void $ runSql $ insert (SubscriptionReminderSent subId reminderId (sid msg))

      -- Mark the reminder as sent
      runSql $ update reminderId [ReminderSent =. True]

    -- Wait a bit and go again
    threadDelay (15*1000*1000) -- 15 mins
