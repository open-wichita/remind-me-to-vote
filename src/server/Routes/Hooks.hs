{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
module Routes.Hooks where

import           Db
import           Types
import           Utils
import           Views

import           ClassyPrelude
import           Data.HVect                (HVect (..), ListContains, findFirst)
import qualified Database.Persist          as Sql
import           Lucid                     (toHtml)
import           Network.HTTP.Types.Status (status403)
import           Web.Spock                 (ClientPreferredFormat (..),
                                            getContext, json, preferredFormat,
                                            readSession, redirect, setStatus,
                                            text)


baseHook :: AppAction () (HVect '[])
baseHook = return HNil


authHook :: AppAction (HVect xs) (HVect ((UserId, User) ': xs))
authHook =
  maybeUser $ \mUser -> do
    oldCtx <- getContext
    case mUser of
      Nothing -> noAccessPage "Unknown user. Login first!"
      Just val -> return (val :&: oldCtx)


data IsAdmin = IsAdmin


adminHook :: ListContains n (UserId, User) xs => AppAction (HVect xs) (HVect (IsAdmin ': xs))
adminHook = do
  (_ :: UserId, user) <- fmap findFirst getContext
  oldCtx <- getContext
  if userRole user == Admin
    then return (IsAdmin :&: oldCtx)
    else noAccessPage "You don't have enough rights, sorry"


data IsGuest = IsGuest


guestOnlyHook :: AppAction (HVect xs) (HVect (IsGuest ': xs))
guestOnlyHook =
  maybeUser $ \mUser -> do
    oldCtx <- getContext
    case mUser of
      Nothing -> return (IsGuest :&: oldCtx)
      Just _ -> redirect "/"


maybeUser :: (Maybe (UserId, User) -> AppAction ctx a) -> AppAction ctx a
maybeUser action = do
  sess <- readSession
  case userId sess of
    Nothing -> action Nothing
    Just uid -> do
      mUser <- runSql $ Sql.get uid
      action (fmap (\user -> (uid, user)) mUser)


noAccessPage :: Text -> AppAction ctx a
noAccessPage msg = do
  setStatus status403
  prefResp <- preferredFormat
  case prefResp of
    PrefJSON -> json msg
    PrefHTML -> lucid $ pageTemplate "No Access" $ toHtml msg
    PrefText -> text msg
    _ -> text msg
