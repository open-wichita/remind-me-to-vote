module Types where

import           Db
import           Settings


import           ClassyPrelude
import           Database.Persist.Sql (SqlBackend)
import           Web.Spock            (SpockActionCtx, SpockCtxM)


type App ctx = SpockCtxM ctx SqlBackend SessionData AppState ()
type AppAction ctx a = SpockActionCtx ctx SqlBackend SessionData AppState a


data SessionData = SessionData
  {
    userId :: Maybe UserId
  }


defaultSessionData :: SessionData
defaultSessionData = SessionData
  {
    userId = Nothing
  }


data AppState = AppState
  {
    appCfg :: AppCfg
  }
