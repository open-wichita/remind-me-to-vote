module Text.Digestive.Spock
  ( runForm
  , postForm'
  ) where

import           Control.Applicative ((<$>))
import           Control.Monad       (return)
import           Control.Monad.Trans (MonadIO)
import           Data.Eq             (Eq ((==)))
import           Data.Function       (const, ($), (.))
import           Data.Functor        (Functor (..))
import qualified Data.HashMap.Strict as HM
import           Data.List           (filter, map, (++))
import           Data.Maybe          (Maybe (..))
import qualified Data.Text           as T
import           Data.Tuple          (fst, snd)
import           Network.HTTP.Types  (methodGet)
import           Network.Wai         (Request (requestMethod))
import           Text.Digestive      (Form, FormInput (FileInput, TextInput),
                                      Path, View, fromPath, getForm, postForm)
import           Web.Spock.Shared    (ActionCtxT,
                                      UploadedFile (uf_tempLocation), files,
                                      params, request)


-- | Run a digestive functors form
runForm
  :: (Functor m, MonadIO m)
  => T.Text -- ^ form name
  -> Form v (ActionCtxT ctx m) a
  -> ActionCtxT ctx m (View v, Maybe a)
runForm formName form = do
  httpMethod <- requestMethod <$> request
  if httpMethod == methodGet
    then do
      f <- getForm formName form
      return (f, Nothing)
    else postForm formName form (const $ return localEnv)


postForm'
  :: (Functor m, MonadIO m)
  => T.Text -- ^ form name
  -> Form v (ActionCtxT ctx m) a
  -> ActionCtxT ctx m (View v, Maybe a)
postForm' formName form =
  postForm formName form (const $ return localEnv)


localEnv
  :: (Functor m, MonadIO m)
  => Path
  -> ActionCtxT ctx m [FormInput]
localEnv path = do
  let name = T.dropAround (== '.') $ fromPath path
      applyParam :: (b -> FormInput) -> [(T.Text, b)] -> [FormInput]
      applyParam f = map (f . snd) . filter ((== name) . fst)
  vars <- applyParam TextInput <$> params
  sentFiles <- (applyParam (FileInput . uf_tempLocation) . HM.toList ) <$> files
  return (vars ++ sentFiles)
