{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
module Actions where

import           Db
import           Db.Models.Subscription    (getSubscriptionsQuery)
import           Forms
import           Forms.Bootstrap3
import           Forms.Utils               (modalForm)
import           Lucid.Bootstrap3
import           Routes.Hooks
import           Settings
import           Text.Digestive.Spock      (postForm', runForm)
import           Twilio.Extended           hiding (runTwilio)
import qualified Twilio.Extended           as Twilio
import           Twilio.Spock
import           Types
import           Users
import           Utils
import           Views


import           ClassyPrelude             hiding (delete)
import           Data.Duration             (humanReadableDuration')
import           Data.HVect                (HVect (..), ListContains, NotInList)
import qualified Data.List                 as List
import           Data.Text                 (strip)
import           Data.Time.Clock           (addUTCTime, diffUTCTime)
import qualified Database.Esqueleto        as E
import           Database.Persist          (Entity (..), Key, SelectOpt (Asc),
                                            ToBackendKey, delete, deleteCascade,
                                            get, getBy, insert, insertMany,
                                            replace, selectFirst, selectList,
                                            update, (=.), (==.), (>=.))
import           Database.Persist.Sql      (SqlBackend, fromSqlKey)
import           Lucid
import           Network.HTTP.Types.Status (status201, status400, status500)
import           Text.Digestive.View       (View (viewErrors), getForm)
import qualified Twilio.Messages           as TwilioMessages
import           Web.Spock                 (getState, html, redirect,
                                            sessionRegenerateId, setStatus,
                                            writeSession)
import qualified Web.Spock                 as Spock


rootAction :: AppAction (HVect xs) a
rootAction = do
  numberFormView <- getForm "newNumberForm" newNumberForm
  emailFormView <- getForm "newEmailForm" newEmailForm
  let formView submitText meta view =
        formWithErrorView submitText Nothing $ renderFormSimple meta (map toHtml view)
  lucid $ pageTemplate "Subscribe" $ do
    h1_ [ class_ "text-center" ] "Remind me to vote!"
    p_ [ class_ "lead text-center" ] "Sign up to receive notifications of when things are happening."
    ul_ [ class_ "nav nav-tabs nav-justified" ] $ do
      li_ [ role_ "presentation", class_ "active" ] (a_ [ href_ "#newNumber", ariaControls_ "newNumber", dataToggle_ "tab" ] "Phone Number")
      li_ [ role_ "presentation" ] (a_ [ href_ "#newEmail", ariaControls_ "newEmail", dataToggle_ "tab" ] "Email")
    div_ [ class_ "tab-content" ] $ do
      div_ [ role_ "tabpanel", class_ "tab-pane active", id_ "newNumber" ] (formView "New Number" newNumberFormMeta numberFormView)
      div_ [ role_ "tabpanel", class_ "tab-pane", id_ "newEmail" ] (formView "New Email" newEmailFormMeta emailFormView)


subscriptionNumberAction :: AppAction (HVect xs) a
subscriptionNumberAction =
  subscriptionNumberActionCommon
    False
    newNumberFormMeta
    pageTemplate
    (p_ "You're subscribed! You will receive a text message shortly with instructions to complete you registration.")


subscriptionNumberActionCommon :: Bool -> FormMeta -> (Text -> Html () -> Html ()) -> Html () -> AppAction (HVect xs) a
subscriptionNumberActionCommon active formMeta template successHtml = do
  f <- runForm "newNumberForm" newNumberForm
  let formView mErr view =
        formWithErrorView "New Number" mErr $ renderFormSimple formMeta (map toHtml view)
      template' = template "Phone Number Subscription"
  case f of
    (view, Nothing) -> do
      unless (null $ viewErrors view) $ setStatus status400
      lucid $ template' (formView Nothing view)
    (view, Just newNumberReq) -> do
      mNumber <- runSql $ getBy (UniqueNumber $ nnrNumber newNumberReq)
      case mNumber of
        Just _ -> lucid $ template' (formView (Just "Number already registered") view)
        Nothing -> do
          subId <- runSql $ insert $ Subscription active
          _ <- runSql $ insert $ buildPreferencesFromRequest subId (nnrPreferences newNumberReq)
          _ <- runSql $ insert $ SubscriptionPhoneNumber subId (nnrNumber newNumberReq)
          unless active $ sendSmsConfirmMessage (nnrNumber newNumberReq)
          setStatus status201
          lucid $ template' successHtml


subscriptionEmailAction :: AppAction (HVect xs) a
subscriptionEmailAction = do
  f <- runForm "newEmailForm" newEmailForm
  let formView mErr view =
        formWithErrorView "New Email" mErr $ renderFormSimple newEmailFormMeta (map toHtml view)
      template = pageTemplate "Email Subscription"
  case f of
    (view, Nothing) -> do
      unless (null $ viewErrors view) $ setStatus status400
      lucid $ template (formView Nothing view)
    (view, Just newEmailReq) -> do
      mEmail <- runSql $ getBy (UniqueEmail $ nerEmail newEmailReq)
      case mEmail of
        Just _ -> lucid $ template (formView (Just "Email already registered") view)
        Nothing -> do
          -- TODO: send request for verification to email to active subscription
          subId <- runSql $ insert $ Subscription False
          _ <- runSql $ insert $ buildPreferencesFromRequest subId (nerPreferences newEmailReq)
          _ <- runSql $ insert $ SubscriptionEmailAddress subId (nerEmail newEmailReq)
          setStatus status201
          lucid $ template (p_ "You're registered")


receiveSmsAction :: AppAction (HVect xs) a
receiveSmsAction = do
  f <- postForm' "" receiveSmsForm
  case f of
    (_, Nothing) -> do
      setStatus status400
      xml "Invalid request."
    (_, Just newReceiveSmsReq) -> do
      twilioAuth <- twilioAuthToken . appTwilioCfg . appCfg <$> getState
      verifiedTwilio <- verifyRequestSpock twilioAuth
      unless verifiedTwilio $ do
        setStatus status400
        xml "Could not verify request"
      receiveSmsCommandAction newReceiveSmsReq


-- TODO: clean this up into a proper parser
receiveSmsCommandAction :: ReceiveSmsRequest -> AppAction (HVect xs) a
receiveSmsCommandAction req
  | cmd `elem` ["subscribe", "signup", "sign up", "start"] = do
      mNumber <- runSql $ getBy (UniqueNumber $ rsrFrom req)
      case mNumber of
        Just _ -> Spock.text "You're already subscribed. Text STOP to unsubscribe."
        Nothing -> do
          subId <- runSql $ insert $ Subscription True
          _ <- runSql $ insert $ Preferences subId True True True True
          _ <- runSql $ insert $ SubscriptionPhoneNumber subId (rsrFrom req)
          Spock.text "You're subscribed to receive reminders about upcoming elections! Text STOP to unsubscribe."
  | cmd `elem` ["unsubscribe", "stop", "stopall", "cancel", "end", "quit", "remove"] = do
      mSubscription <- runSql $ getBy (UniqueNumber $ rsrFrom req)
      case mSubscription of
        Nothing -> html "You were not subscribed. TEXT SIGNUP to subscribe."
        Just (Entity _ subNumber) -> do
          _ <- runSql $ deleteCascade (subscriptionPhoneNumberSubscription subNumber)
          Spock.text "You're unsubscribed. Text SIGNUP to resubscribe for reminders about upcoming elections."
  | cmd `elem` ["confirm"] = do
      mNumber <- runSql $ getBy (UniqueNumber $ rsrFrom req)
      case mNumber of
        Nothing -> Spock.text "You're not subscribed yet. Text SUBSCRIBE if you'd like to subscribe for reminders about upcoming elections."
        Just (Entity _ subNumber) -> do
          _ <- runSql $ update (subscriptionPhoneNumberSubscription subNumber) [SubscriptionActive =. True]
          Spock.text "You're confirmed to receive reminders about upcoming elections! Text STOP to unsubscribe."
  | otherwise =
      Spock.text "Unknown command. Try again. Text SIGNUP to subscribe or STOP to unsubscribe."
  where
    cmd = toLower . strip $ rsrBody req


sendSmsConfirmMessage :: Text -> AppAction (HVect xs) ()
sendSmsConfirmMessage number = do
  twilioCfg <- appTwilioCfg . appCfg <$> getState
  let msg = TwilioMessages.PostMessage
        { sendFrom = twilioNumber twilioCfg
        , sendTo = number
        , sendBody = "Reply with CONFIRM to activate your subscription. You will not receive reminders until you complete this step."
        }
  _ <- runTwilio $ TwilioMessages.post msg
  return ()


registerAction :: (ListContains n IsGuest xs, NotInList (UserId, User) xs ~ 'True) => AppAction (HVect xs) a
registerAction = do
  f <- runForm "registerForm" registerForm
  let formView mErr view =
        formWithErrorView "Register" mErr $ renderFormSimple registerFormMeta (map toHtml view)
      template = pageTemplate "Register Account"
  case f of
    (view, Nothing) -> do
      unless (null $ viewErrors view) $ setStatus status400
      lucid $ template (formView Nothing view)
    (view, Just registerReq) ->
      if rrPassword registerReq /= rrPasswordConfirm registerReq
        then do
          setStatus status400
          lucid $ template (formView (Just "Passwords do not match.") view)
        else do
          registerRes <- runSql $ registerUser (rrUsername registerReq) (rrPassword registerReq)
          case registerRes of
            Left errMsg -> do
              setStatus status400
              lucid $ template (formView (Just errMsg) view)
            Right _ -> do
              setStatus status201
              lucid $ template (p_ $ "Account created. You may now " >> a_ [href_ "/login"] "login")


loginAction :: (ListContains n IsGuest xs, NotInList (UserId, User) xs ~ 'True) => AppAction (HVect xs) a
loginAction = do
  f <- runForm "loginForm" loginForm
  let formView mErr view =
        formWithErrorView "Login" mErr $ renderFormSimple loginFormMeta (map toHtml view)
      template = pageTemplate "Login"
  case f of
    (view, Nothing) -> do
      unless (null $ viewErrors view) $ setStatus status400
      lucid $ template (formView Nothing view)
    (view, Just loginReq) -> do
      loginRes <- runSql $ loginUser (lrUsername loginReq) (lrPassword loginReq)
      case loginRes of
        Just (Entity userId user) -> do
          sessionRegenerateId
          writeSession $ defaultSessionData { userId = Just userId }
          if userRole user == Admin
             then redirect "/admin"
             else redirect "/"
        Nothing -> lucid $ template (formView (Just "Invalid login credentials") view)


logoutAction:: ListContains n (UserId, User) xs => AppAction (HVect xs) a
logoutAction = do
  writeSession defaultSessionData
  redirect "/"


adminRootAction :: ListContains n IsAdmin xs => AppAction (HVect xs) a
adminRootAction = do
  events <- runSql $
    E.select $
      E.from $ \(e `E.LeftOuterJoin` mer) -> do
        E.on (E.just (E.just (e E.^. EventId)) E.==. mer E.?. ReminderEvent)
        E.orderBy [E.asc (e E.^. EventOccurs)]
        E.limit 5
        return (e, mer)
  lucid $ adminPageTemplate "Admin Dashboard" $ do
    h2_ "Upcoming events"
    adminEventsListView (mkEventView events)


adminEventsAction :: ListContains n IsAdmin xs => AppAction (HVect xs) a
adminEventsAction = do
  f <- runForm "newEventForm" $ newEventForm Nothing
  let formView mErr view =
        formWithErrorView "New Event" mErr $ renderFormSimple newEventFormMeta (map toHtml view)
      template = adminPageTemplate "Events"
  case f of
    (view, Nothing) -> do
      unless (null $ viewErrors view) $ setStatus status400
      if null $ viewErrors view
         then adminEventsActionList (map toHtml view)
         else lucid $ template (formView Nothing view)
    (view, Just newEventReq) -> do
      now <- liftIO getCurrentTime
      let event = Event (nerName newEventReq) (nerOccurs newEventReq)
      eventId <- runSql $ insert event
      let reminders = generateDefaultRemindersForEvent (Entity eventId event) now
      rIds <- runSql $ insertMany reminders
      let reminderEntities = zipWith Entity rIds reminders
      setStatus status201
      lucid $ adminSingleEventView (EventView (Entity eventId event) reminderEntities) view


adminEventsActionList :: View (Html ()) -> AppAction ctx a
adminEventsActionList view = do
  -- TODO: query param to hide/show past events
  events <- runSql $
    E.select $
      E.from $ \(e `E.LeftOuterJoin` mer) -> do
        E.on (E.just (E.just (e E.^. EventId)) E.==. mer E.?. ReminderEvent)
        E.orderBy [E.asc (e E.^. EventId)]
        return (e, mer)
  lucid $ adminPageTemplate "Events" (do
      rmtvPageHeader_ "Events" [modalForm "New Event" newEventFormMeta view]
      adminEventsListView (mkEventView events)
    )


data EventView = EventView
  { evEvent     :: Entity Event
  , evReminders :: [Entity Reminder]
  }


mkEventView :: [(Entity Event, Maybe (Entity Reminder))] -> [EventView]
mkEventView = map mkSingleEventView . groupBy ((==) `on` entityKey . fst)


mkSingleEventView :: [(Entity Event, Maybe (Entity Reminder))] -> EventView
mkSingleEventView pairs =
  EventView
    { evEvent = event
    , evReminders = reminders
    }
  where
    event = (fst . List.head) pairs
    reminders = mapMaybe snd pairs


adminEventsListView :: [EventView] -> Html ()
adminEventsListView events =
  tableResponsive_
    (tr_ $ th_ "Name" >> th_ "Occurs" >> th_ "Actions")
    (forM_ events $
      \EventView { evEvent = (Entity eventId Event {..})} ->
        tr_ $ do
          td_ (toHtml eventName)
          td_ (toHtml (formatTime defaultTimeLocale "%Y-%m-%dT%H:%M:%S%z" eventOccurs))
          td_ (entityActions "/admin/events/" eventId)
    )


adminSingleEventAction :: ListContains n IsAdmin xs => EventId -> AppAction (HVect xs) a
adminSingleEventAction eventId = do
  event <- get404 eventId
  (view, mReq) <- runForm "updateEventForm" $ newEventForm (Just event)
  case mReq of
    Nothing -> do
      unless (null $ viewErrors view) $ setStatus status400
      displayEvent event view
    (Just newEventReq) -> do
      let event' = Event (nerName newEventReq) (nerOccurs newEventReq)
      runSql $ replace eventId event'
      displayEvent event' view
  where
    displayEvent event view = do
      reminderEntities <- runSql $ selectList [ReminderEvent ==. Just eventId] [Asc ReminderOccurs]
      lucid $ adminSingleEventView (EventView (Entity eventId event) reminderEntities) view


adminSingleEventView :: EventView -> View Text -> Html ()
adminSingleEventView EventView { evEvent = eventEntity@(Entity eId Event {..}), evReminders = reminderEntities } view =
 adminPageTemplate (eventName <> " Event") (do
   pageHeader_ "Event"
   renderForm (updateEventFormMeta eId) (toHtml <$> view)
   reminderView <- getForm "newEventReminderForm" $ newReminderForm [eventEntity] Nothing
   rmtvPageHeader_ "Event Reminders" [modalForm "New Event Reminder" newReminderFormMeta (map toHtml reminderView) ]
   tableResponsive_
     (tr_ $ th_ "Occurs" >> th_ "Message" >> th_ "Sent" >> th_ "Actions")
     (forM_ reminderEntities $
       \(Entity rId Reminder {..}) ->
         tr_ $ do
           td_ (toHtml $ formatTime defaultTimeLocale "%Y-%m-%dT%H:%M:%S%z" reminderOccurs)
           td_ (toHtml reminderMessage)
           td_ (if reminderSent then "Yes" else "No")
           td_ (entityActions "/admin/reminders/" rId)
     )
   )


adminDeleteSingleEventAction :: ListContains n IsAdmin xs => EventId -> AppAction (HVect xs) a
adminDeleteSingleEventAction eventId = do
  event <- get404 eventId
  runSql $ deleteCascade eventId
  lucid $ adminPageTemplate "Event Deleted" $ toHtml ("Event \"" <> eventName event <> "\" deleted")


adminRemindersAction :: ListContains n IsAdmin xs => AppAction (HVect xs) a
adminRemindersAction = do
  now <- liftIO getCurrentTime
  eventEntities <- runSql $ selectList [EventOccurs >=. now] []
  f <- runForm "newReminderForm" $ newReminderForm eventEntities Nothing
  let formView mErr view =
        formWithErrorView "New Reminder" mErr $ renderFormSimple newReminderFormMeta (map toHtml view)
      template = adminPageTemplate "Reminders"
  case f of
    (view, Nothing) -> do
      unless (null $ viewErrors view) $ setStatus status400
      if null $ viewErrors view
         then adminRemindersActionList (map toHtml view)
         else lucid $ template (formView Nothing view)
    (view, Just NewReminderRequest {..}) ->
      if now >= nrrOccurs
         then do
            setStatus status400
            lucid $ template (formView (Just "Event must occur in the future") view)
         else do
            let reminder = Reminder nrrOccurs nrrMessage False nrrEvent
            reminderId <- runSql $ insert reminder
            mEvent <- maybe (return Nothing) (runSql . get) nrrEvent
            setStatus status201
            lucid $ adminSingleReminderView (Entity reminderId reminder) mEvent view


adminRemindersActionList :: View (Html ()) -> AppAction ctx a
adminRemindersActionList view = do
  reminders <- runSql $
    E.select $
      E.from $ \(r `E.LeftOuterJoin` me) -> do
        E.on (r E.^. ReminderEvent E.==. me E.?. EventId)
        E.where_ (r E.^. ReminderSent E.==. E.val False) -- TODO: parameterize this
        E.orderBy [E.asc (r E.^. ReminderId)]
        return (r, me)
  lucid $ adminPageTemplate "Reminders" (do
      rmtvPageHeader_ "Reminders" [modalForm "New Reminder" newReminderFormMeta view]
      tableResponsive_
        (tr_ $ th_ "Event" >> th_ "Occurs" >> th_ "Actions")
        (forM_ reminders $
          \(Entity rId Reminder {..}, mEvent) ->
            tr_ $ do
              td_ (maybe "None (One off)" (toHtml . eventName . entityVal) mEvent)
              td_ (toHtml (formatTime defaultTimeLocale "%Y-%m-%dT%H:%M:%S%z" reminderOccurs))
              td_ (entityActions "/admin/reminders/" rId)
        )
    )


adminSingleReminderAction :: ListContains n IsAdmin xs => ReminderId -> AppAction (HVect xs) a
adminSingleReminderAction reminderId = do
  reminder <- get404 reminderId
  now <- liftIO getCurrentTime
  eventEntities <- runSql $ selectList [EventOccurs >=. now] []
  (view, mReq) <- runForm "updateReminderForm" $ newReminderForm eventEntities (Just reminder)
  case (mReq, reminderSent reminder) of
    (Nothing, _) -> do
      unless (null $ viewErrors view) $ setStatus status400
      displayReminder reminder view
    (_, True) -> do
      -- reject the update if the reminder has already been sent
      setStatus status400
      displayReminder reminder view
    (Just NewReminderRequest {..}, False) -> do
      let reminder' = reminder { reminderOccurs = nrrOccurs
                               , reminderMessage = nrrMessage
                               , reminderEvent = nrrEvent
                               }
      runSql $ replace reminderId reminder'
      displayReminder reminder' view
  where
    displayReminder reminder view = do
      mEvent <- maybe (return Nothing) (runSql . get) (reminderEvent reminder)
      lucid $ adminSingleReminderView (Entity reminderId reminder) mEvent view


adminSingleReminderView :: Entity Reminder -> Maybe Event -> View Text -> Html ()
adminSingleReminderView (Entity rId Reminder {..}) mEvent view =
  adminPageTemplate "Reminder" (do
    pageHeader_ "Reminder"
    row_ $ do
      colXs_ 6 $
        case mEvent of
          Just Event {..} -> do
            a_ [ href_ ("/admin/events/" <> tshow (fromSqlKey rId)) ] $ toHtml eventName
            p_ $ toHtml (formatTime defaultTimeLocale "%Y-%m-%dT%H:%M:%S%z" eventOccurs)
          Nothing ->
            p_ "One off reminder"
      colXs_ 6 $
        p_ $ "Sent: " ++ (if reminderSent then "Yes" else "No")
    renderForm ((updateReminderFormMeta rId) { fmDisabled = reminderSent }) (map toHtml view)
  )


adminDeleteSingleReminderAction :: ListContains n IsAdmin xs => ReminderId -> AppAction (HVect xs) a
adminDeleteSingleReminderAction reminderId = do
  reminder <- get404 reminderId
  runSql $ delete reminderId
  lucid $ adminPageTemplate "Reminder Deleted" $ toHtml ("Reminder \"" <> reminderMessage reminder <> "\" deleted")


-- TODO: display emails
adminSubscriptionsAction :: ListContains n IsAdmin xs => AppAction (HVect xs) a
adminSubscriptionsAction = do
  subs <- runSql getSubscriptionsQuery
  lucid $ adminPageTemplate "Subscriptions" (do
      pageHeader_ "Subscriptions"
      -- TODO: add new subscription button/dropdown
      tableResponsive_
        (tr_ $ th_ "Number" >> th_ "Active" >> th_ "Actions")
        (forM_ subs $
          \(Entity subId Subscription {..}, Entity _ SubscriptionPhoneNumber {..}) ->
            tr_ $ do
              td_ (toHtml subscriptionPhoneNumberNumber)
              td_ (if subscriptionActive then "Yes" else "No")
              td_ (a_ [ href_ ("/admin/subscriptions/" <> tshow (fromSqlKey subId)) ] "Edit")
        )
    )


adminSingleSubscriptionAction :: ListContains n IsAdmin xs => SubscriptionId -> AppAction (HVect xs) a
adminSingleSubscriptionAction subId = do
  sub <- get404 subId
  mSubPhone <- runSql $ selectFirst [SubscriptionPhoneNumberSubscription ==. subId] []
  mSubEmail <- runSql $ selectFirst [SubscriptionEmailAddressSubscription ==. subId] []
  case (sub, mSubPhone, mSubEmail) of
    (Subscription {..}, Just (Entity _ SubscriptionPhoneNumber {..}) , _) ->
      lucid $ adminPageTemplate "Subscription" (do
          pageHeader_ "Subscription"
          -- TODO: make editable
          p_ (if subscriptionActive then "Yes" else "No")
          p_ $ toHtml subscriptionPhoneNumberNumber
        )

    (Subscription {..}, _, Just (Entity _ SubscriptionEmailAddress {..})) ->
      lucid $ adminPageTemplate "Subscription" (do
          pageHeader_ "Subscription"
          -- TODO: make editable
          p_ (if subscriptionActive then "Yes" else "No")
          p_ $ toHtml subscriptionEmailAddressAddress
        )

    (Subscription {..}, Nothing, Nothing) -> do
      setStatus status500
      lucid $ adminPageTemplate "Subscription" (do
          pageHeader_ "Subscription"
          p_ "Could not find an associated subscription"
        )


adminSubscriptionNumbersAction :: ListContains n IsAdmin xs => AppAction (HVect xs) a
adminSubscriptionNumbersAction =
  subscriptionNumberActionCommon
    True
    (newNumberFormMeta { fmTarget = "/admin/subscriptions/numbers" })
    adminPageTemplate
    (p_ "Number is registered and active.")


-- TODO: finish
adminSubscriptionEmailsAction :: ListContains n IsAdmin xs => AppAction (HVect xs) a
adminSubscriptionEmailsAction = undefined


generateDefaultRemindersForEvent :: Entity Event -> UTCTime -> [Reminder]
generateDefaultRemindersForEvent (Entity eventId Event {..}) now = catMaybes
  -- TODO: between now and eventOccurs, generate a few reminders
  [ hourBefore
  ]
  where
    hourBefore = reminderForTime $ addUTCTime (-1*60*60) eventOccurs
    reminderForTime time = if time < now
                            then Nothing
                            else Just $ Reminder time (messageForTime time) False (Just eventId)
    messageForTime time = eventName <> " begins in about " <> (pack . humanReadableDuration' $ diffUTCTime eventOccurs time)


runTwilio :: Twilio a -> AppAction ctx a
runTwilio action = do
  twilioCfg <- appTwilioCfg . appCfg <$> getState
  liftIO $ Twilio.runTwilio (twilioAuthId twilioCfg, twilioAuthToken twilioCfg) action


rmtvPageHeader_ :: Html () -> [Html ()] -> Html ()
rmtvPageHeader_ name actions =
  div_ [class_ "rmtv-page-header"] $ do
    h1_ [class_ "rmtv-page-header-item"] name
    div_ [class_ "rmtv-page-header-actions"] $
      forM_ actions $ \action -> div_ [class_ "rmtv-page-header-item"] action


entityActions :: (ToBackendKey SqlBackend a) => Text -> Key a -> Html ()
entityActions baseUrl entityId =
  div_ [ class_ "btn-group", role_ "group" ] $ do
    a_ [ role_ "button", class_ "btn btn-default", href_ (baseUrl <> tshow (fromSqlKey entityId)) ] "Edit"
    button_ [ role_ "button", class_ "btn btn-default btn-delete", dataTarget_ (baseUrl <> tshow (fromSqlKey entityId) <> "/?_method=DELETE") ] "Delete"
