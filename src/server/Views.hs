module Views where

import           Lucid.Bootstrap3

import           ClassyPrelude
import           Lucid
import           Lucid.Base       (makeAttribute)


pageTemplate :: Text -> Html () -> Html ()
pageTemplate = pageTemplateBase mempty


adminPageTemplate :: Text -> Html () -> Html ()
adminPageTemplate =
  pageTemplateBase
    (mainNavigation_ "/admin" "RMTV"
      [
        ("/admin/events", "Events")
      , ("/admin/subscriptions", "Subscriptions")
      , ("/admin/reminders", "Reminders")
      ]
      [
        ("/logout", "Logout")
      ]
    )


pageTemplateBase :: Html () -> Text -> Html () -> Html ()
pageTemplateBase header title contents = doctypehtml_
  (do
    head_ (do
            meta_ [ charset_ "utf-8" ]
            meta_ [ name_ "viewport"
                  , content_ "width=device-width, initial-scale=1"
                  ]
            link_ [ href_ "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
                  , rel_ "stylesheet"
                  , type_ "text/css"
                  , makeAttribute "integrity" "sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
                  , makeAttribute "crossorigin" "anonymous"
                  ]
            stylesheet "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css"
            stylesheet "/css/main.css"
            title_ $ toHtml (title <> " | Remind Me To Vote")
          )
    body_ $ do
      header
      container_ $ do
        contents
        footer_ [ class_ "footer" ] $
          p_ "We do not share your personal information with anyone and will not send unsolicited messages."
      javascriptFile "https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"
      script_ [ src_ "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
              , makeAttribute "integrity" "sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
              , makeAttribute "crossorigin" "anonymous"
              ] ("" :: Text)
      javascriptFile "https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js"
      javascriptFile "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"
      javascriptFile "/js/app.js"
  )


formWithErrorView :: Text -> Maybe Text -> Html () -> Html ()
formWithErrorView _ mError ct = do
  case mError of
    Just errMsg ->
      alert_ BootAlertDanger (toHtml errMsg)
    Nothing -> mempty
  div_ ct
