{-# LANGUAGE MultiParamTypeClasses #-}
module Lucid.Bootstrap3 where

import           Control.Monad (Monad, forM_, unless)
import           Data.Bool     (Bool (..))
import           Data.Eq       (Eq ((==)))
import           Data.Function (($), (.))
import           Data.Int      (Int)
import           Data.List     (null)
import           Data.Maybe    (Maybe (..))
import           Data.Monoid   (Monoid (mempty), (<>))
import           Data.Text     (Text, pack)
import qualified Data.Text     as T
import           Lucid.Base
import           Lucid.Html5
import           Text.Show     (Show (show))


-- ELEMENTS


container_ :: Term arg result => arg -> result
container_ =
  termWith "div" [class_ " container "]


row_ :: Term arg result => arg -> result
row_ =
  termWith "div" [class_ " row "]


colXs_, colSm_, colMd_, colLg_ :: Term arg result => Int -> arg -> result
colXs_ = col'_ "xs"
colSm_ = col'_ "sm"
colMd_ = col'_ "md"
colLg_ = col'_ "lg"


col'_ :: Term arg result => Text -> Int -> arg -> result
col'_ size num =
  termWith "div" [class_ (" col-" <> size <> "-" <> (pack . show $ num ) <> " ")]


data BootAlertType
   = BootAlertDanger
   | BootAlertWarn
   | BootAlertInfo
   | BootAlertSuccess


-- TODO: make a Term?
alert_ :: Monad m => BootAlertType -> HtmlT m () -> HtmlT m ()
alert_ alertType alertVal =
  div_ [class_ ("alert alert-dismissable " <> t)] $ do
     button_ [type_ "button", class_ "close", dataDismiss_ "alert", ariaHidden_ True] htmlTimesSymbol
     alertVal
  where
    t =
        case alertType of
          BootAlertDanger -> "alert-danger"
          BootAlertWarn -> "alert-warning"
          BootAlertInfo -> "alert-info"
          BootAlertSuccess -> "alert-success"


pageHeader_ :: Monad m => HtmlT m () -> HtmlT m ()
pageHeader_ header =
  div_ [class_ "page-header"] $
    h1_ header


mainNavigation_
  :: Monad m
  => Text
  -> HtmlT m ()
  -> [(Text, HtmlT m ())] -- ^ Left side nav
  -> [(Text, HtmlT m ())] -- ^ Right side nav
  -> HtmlT m ()
mainNavigation_ indexPath pageTitle mainNavPoints rightNav =
  nav_ [class_ "navbar navbar-default navbar-static-top"] $
    container_ $ do
      div_ [class_ "navbar-header"] $ do
        button_ [type_ "button", class_ "navbar-toggle", dataToggle_ "collapse", dataTarget_ "#main-nav"] $ do
          span_ [class_ "sr-only"] "Toggle navigation"
          span_ [class_ "icon-bar"] emptyEl
          span_ [class_ "icon-bar"] emptyEl
          span_ [class_ "icon-bar"] emptyEl
        a_ [class_ "navbar-brand", href_ indexPath] pageTitle
      div_ [class_ "collapse navbar-collapse", id_ "main-nav"] $ do
        ul_ [class_ "nav navbar-nav"] $
          forM_ mainNavPoints $ \(url, val) ->
            li_ (a_ [href_ url] val)
        unless (null rightNav) $
          ul_ [class_ "nav navbar-nav navbar-right"] $
            forM_ rightNav $ \(url, val) ->
              li_ (a_ [href_ url] val)


formGroup_ :: Term arg result => arg -> result
formGroup_ =
  termWith "div" [class_ " form-group "]


formSelect_
  :: Monad m
  => Text
  -> Text
  -> [(Text, HtmlT m ())]
  -> Maybe Text
  -> HtmlT m ()
formSelect_ selLabel selName keyValues selectedK =
  formGroup_ $ do
    label_ [for_ selName] (toHtml selLabel)
    select_ [name_ selName, class_ "form-control"] $
      forM_ keyValues $ \(k, v) ->
        option_ (attrs k)  v
  where
    attrs k = [value_ k] <>
      if Just k == selectedK
        then [selected_ "selected"]
        else mempty


formSubmit_ :: Term arg result => arg -> result
formSubmit_ =
  termWith "button"
    [
      type_ "submit"
    , class_ " btn btn-lg btn-success btn-block "
    ]


tableResponsive_ :: Monad m => HtmlT m () -> HtmlT m () -> HtmlT m ()
tableResponsive_ tblHead tblBody =
  div_ [class_ "table-responsive"] $
    table_ [class_ "table table-striped table-bordered table-hover"] $ do
      thead_ tblHead
      tbody_ tblBody


-- ATTRIBUTES


dataToggle_ :: Text -> Attribute
dataToggle_ = data_ "toggle"


dataTarget_ :: Text -> Attribute
dataTarget_ = data_ "target"


dataDismiss_ :: Text -> Attribute
dataDismiss_ = data_ "dismiss"


ariaControls_ :: Text -> Attribute
ariaControls_ =
  makeAttribute "aria-controls"


ariaHidden_ :: Bool -> Attribute
ariaHidden_ bool =
  makeAttribute "aria-hidden" (if bool then "true" else "false")


ariaLabel_ :: Text -> Attribute
ariaLabel_ =
  makeAttribute "aria-label"


-- HELPERS


stylesheet :: Monad m => Text -> HtmlT m ()
stylesheet path =
  link_
    [
      href_ path
    , rel_ "stylesheet"
    , type_ "text/css"
    ]


javascriptFile :: Monad m => Text -> HtmlT m ()
javascriptFile path = script_ [src_ path] T.empty


emptyEl :: Monad m => HtmlT m ()
emptyEl = mempty


htmlTimesSymbol :: Monad m => HtmlT m ()
htmlTimesSymbol = toHtmlRaw ("&times;" :: Text)
