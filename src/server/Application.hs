module Application where

import           Db                                    (migrateAll)
import           Routes
import           Routes.Hooks                          (baseHook)
import           Settings                              (AppCfg (..),
                                                        configSettingsYmlValue)
import           Types


import           ClassyPrelude
import           Control.Monad.Logger                  (runStdoutLoggingT)
import           Database.Persist.Postgresql           (SqlBackend,
                                                        createPostgresqlPool,
                                                        pgConnStr, pgPoolSize,
                                                        runMigration,
                                                        runSqlPool)
import           MultiSourceConfig                     (configSettingsYml,
                                                        loadAppSettings,
                                                        loadAppSettingsArgs,
                                                        useEnv)
import           Network.Wai                           (Application, Middleware)
import           Network.Wai.Handler.Warp              (Settings,
                                                        defaultSettings,
                                                        getPort, runSettings,
                                                        setHost, setPort)
import           Network.Wai.Middleware.AcceptOverride (acceptOverride)
import           Network.Wai.Middleware.Autohead       (autohead)
import           Network.Wai.Middleware.Gzip           (def, gzip)
import           Network.Wai.Middleware.MethodOverride (methodOverride)
import           Network.Wai.Middleware.RequestLogger  (logStdout)
import           Network.Wai.Middleware.Static         (addBase, staticPolicy)
import           Web.Spock.Safe                        (PoolOrConn (PCPool),
                                                        SpockCfg (..),
                                                        defaultSpockCfg,
                                                        getState, middleware,
                                                        prehook, spock,
                                                        spockAsApp)


-- | The @main@ function for an executable running this site.
appMain :: IO ()
appMain = do
  -- Get the settings from all relevant sources
  appCfg' <- loadAppSettingsArgs
    -- fall back to compile-time values, set to [] to require values at runtime
    [configSettingsYmlValue]

    -- allow environment variables to override
    useEnv

  spockCfg <- initializeApp appCfg'

  -- Generate a WAI Application from our settings
  app <- makeApplication appCfg' spockCfg

  -- Run the application with Warp
  runSettings (warpSettings appCfg') app


-- | This function allocates resources (such as a database connection pool),
-- performs initialization and return a foundation data type value.
initializeApp :: AppCfg -> IO (SpockCfg SqlBackend SessionData AppState)
initializeApp appCfg' = do
  -- Create the database connection pool
  pool <- runStdoutLoggingT $ createPostgresqlPool
    (pgConnStr  $ appDatabaseCfg appCfg')
    (pgPoolSize $ appDatabaseCfg appCfg')

  -- Perform database migration
  runStdoutLoggingT $ runSqlPool (runMigration migrateAll) pool

  return $ defaultSpockCfg defaultSessionData (PCPool pool) (AppState appCfg')


-- | Create a WAI Application using our settings and apply some additional
-- middlewares.
makeApplication :: AppCfg -> SpockCfg SqlBackend SessionData AppState -> IO Application
makeApplication _ spockCfg = do
  -- Create the WAI application and apply middlewares
  app <- spockAsApp (spock spockCfg spockApp)
  return $ logStdout $ defaultMiddlewaresNoLogging app


spockApp :: App ()
spockApp =
  prehook baseHook $ do
    staticDir <- appStaticDir . appCfg <$> getState
    middleware (staticPolicy (addBase (unpack staticDir)))
    routes


-- | Make Warp settings for the given app settings.
warpSettings :: AppCfg -> Settings
warpSettings appCfg' =
    setPort (appPort appCfg')
  $ setHost (appHost appCfg')
    defaultSettings


-- | All of the default middlewares, excluding logging.
defaultMiddlewaresNoLogging :: Middleware
defaultMiddlewaresNoLogging = acceptOverride . autohead . gzip def . methodOverride


--------------------------------------------------------------
-- Functions for DevelMain.hs (a way to run the app from GHCi)
--------------------------------------------------------------
getApplicationRepl :: IO (Int, Application)
getApplicationRepl = do
  cfg <- loadAppSettings [configSettingsYml] [] useEnv
  spockCfg <- initializeApp cfg
  let wsettings = warpSettings cfg
  waiApp <- makeApplication cfg spockCfg
  return (getPort wsettings, waiApp)


shutdownApp :: IO ()
shutdownApp = return ()
