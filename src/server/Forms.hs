{-# LANGUAGE RecordWildCards #-}
 module Forms where

import           Db
import           Forms.Bootstrap3
import           Forms.Utils          (optionalChoice, phoneNumberFormlet,
                                       utcDateTimeFormlet)


import           ClassyPrelude        hiding (bool)
import qualified Database.Persist     as Sql
import           Database.Persist.Sql (fromSqlKey, toSqlKey)
import           Text.Digestive       (Form, bool, check, text, (.:))
import           Text.Email.Validate  (isValid)


preferencesFormElements :: [FormElement]
preferencesFormElements =
  [
    FormElement "localElections" "Local Elections" InputCheckbox False False
  , FormElement "stateElections" "State Elections" InputCheckbox False False
  , FormElement "federalElections" "Federal Elections" InputCheckbox False False
  , FormElement "registrationDeadlines" "Registration Deadlines" InputCheckbox False False
  ]


data NewPreferencesRequest = NewPreferencesRequest
  {
    nprLocalElections        :: Bool
  , nprStateElections        :: Bool
  , nprFederalElections      :: Bool
  , nprRegistrationDeadlines :: Bool
  }


newPreferencesForm :: Monad m => Form Text m NewPreferencesRequest
newPreferencesForm =
  NewPreferencesRequest
    <$> "localElections" .: bool (Just False)
    <*> "stateElections" .: bool (Just False)
    <*> "federalElections" .: bool (Just False)
    <*> "registrationDeadlines" .: bool (Just False)


buildPreferencesFromRequest :: Sql.Key Subscription -> NewPreferencesRequest -> Preferences
buildPreferencesFromRequest subId prefs =
  Preferences
    {
      preferencesSubscription = subId
    , preferencesLocalElections = nprLocalElections prefs
    , preferencesStateElections = nprStateElections prefs
    , preferencesFederalElections = nprFederalElections prefs
    , preferencesRegistrationDeadlines = nprRegistrationDeadlines prefs
    }


data NewNumberRequest = NewNumberRequest
  {
    nnrNumber      :: Text
  , nnrPreferences :: NewPreferencesRequest
  }


newNumberForm :: Monad m => Form Text m NewNumberRequest
newNumberForm =
  NewNumberRequest
    <$> "number" .: phoneNumberFormlet Nothing
    <*> newPreferencesForm


newNumberFormMeta :: FormMeta
newNumberFormMeta = FormMeta
  { fmMethod = POST
  , fmTarget = "/subscriptions/numbers"
  , fmElements =
    [ FormElement "number" "Phone Number" InputTelephone True False
    ] <> preferencesFormElements
  , fmSubmitText = "Add Number"
  , fmDisabled = False
  }


data NewEmailRequest = NewEmailRequest
  {
    nerEmail       :: Text
  , nerPreferences :: NewPreferencesRequest
  }


newEmailForm :: Monad m => Form Text m NewEmailRequest
newEmailForm =
  NewEmailRequest
    <$> "email" .: check "Not a valid email address" checkEmail (text Nothing)
    <*> newPreferencesForm


checkEmail :: Text -> Bool
checkEmail = isValid . encodeUtf8


newEmailFormMeta :: FormMeta
newEmailFormMeta = FormMeta
  { fmMethod = POST
  , fmTarget = "/subscriptions/emails"
  , fmElements =
    [ FormElement "email" "Email Address" InputEmail True False
    ] <> preferencesFormElements
  , fmSubmitText = "Add Email"
  , fmDisabled = False
  }


data LoginRequest = LoginRequest
  {
    lrUsername :: Text
  , lrPassword :: Text
  }


loginForm :: Monad m => Form Text m LoginRequest
loginForm =
  LoginRequest
    <$> "username" .: text Nothing
    <*> "password" .: text Nothing


loginFormMeta :: FormMeta
loginFormMeta = FormMeta
  { fmMethod = POST
  , fmTarget = "/login"
  , fmElements =
    [ FormElement "username" "Username" InputText True False
    , FormElement "password" "Password" InputPassword True False
    ]
  , fmSubmitText = "Login"
  , fmDisabled = False
  }


data RegisterRequest = RegisterRequest
  {
    rrUsername        :: Text
  , rrPassword        :: Text
  , rrPasswordConfirm :: Text
  }


registerForm :: Monad m => Form Text m RegisterRequest
registerForm =
  RegisterRequest
    <$> "username" .: text Nothing
    <*> "password" .: text Nothing
    <*> "passwordConfirm" .: text Nothing


registerFormMeta :: FormMeta
registerFormMeta = FormMeta
  { fmMethod = POST
  , fmTarget = "/register"
  , fmElements =
    [ FormElement "username" "Username" InputText True False
    , FormElement "password" "Password" InputPassword True False
    , FormElement "passwordConfirm" "Repeat Password" InputPassword True False
    ]
  , fmSubmitText = "Register"
  , fmDisabled = False
  }


data ReceiveSmsRequest = ReceiveSmsRequest
  {
    rsrFrom :: Text
  , rsrTo   :: Text
  , rsrBody :: Text
  } deriving (Show)


receiveSmsForm :: Monad m => Form Text m ReceiveSmsRequest
receiveSmsForm =
  ReceiveSmsRequest
    <$> "From" .: phoneNumberFormlet Nothing
    <*> "To" .: phoneNumberFormlet Nothing
    <*> "Body" .: check "Body must not be empty" (not . null) (text Nothing)


data NewEventRequest = NewEventRequest
  {
    nerName   :: Text
  , nerOccurs :: UTCTime
  }


newEventForm :: Monad m => Maybe Event -> Form Text m NewEventRequest
newEventForm mEvent =
  NewEventRequest
    <$> "name" .: text (map eventName mEvent)
    <*> "occurs" .: utcDateTimeFormlet "%Y-%m-%dT%H:%M:%S%z" (map eventOccurs mEvent)


newEventFormMeta :: FormMeta
newEventFormMeta = FormMeta
  { fmMethod = POST
  , fmTarget = "/admin/events/"
  , fmElements =
    [ FormElement "name" "Event Name" InputText True False
    , FormElement "occurs" "Event Time" InputDateTime True False
    ]
  , fmSubmitText = "Create Event"
  , fmDisabled = False
}


updateEventFormMeta :: Sql.Key Event -> FormMeta
updateEventFormMeta eId = newEventFormMeta
  { fmMethod = PUT
  , fmTarget = "/admin/events/" <> tshow (fromSqlKey eId)
  , fmSubmitText = "Update Event"
  }


data NewReminderRequest = NewReminderRequest
  {
    nrrMessage :: Text
  , nrrOccurs  :: UTCTime
  , nrrEvent   :: Maybe (Sql.Key Event)
  }


newReminderForm :: Monad m => [Sql.Entity Event] -> Maybe Reminder -> Form Text m NewReminderRequest
newReminderForm events mReminder =
  NewReminderRequest
    <$> "message" .: text (map reminderMessage mReminder)
    <*> "occurs" .: utcDateTimeFormlet "%Y-%m-%dT%H:%M:%S%z" (map reminderOccurs mReminder)
    <*> "event" .: map (map toSqlKey) (optionalChoice eventChoices (map reminderEventId mReminder))
  where
    eventChoices = (0, "None") : map (\(Sql.Entity eId Event {..}) -> (fromSqlKey eId, eventName)) events
    reminderEventId :: Reminder -> Int64
    reminderEventId = maybe 0 fromSqlKey . reminderEvent


newReminderFormMeta :: FormMeta
newReminderFormMeta = FormMeta
  { fmMethod = POST
  , fmTarget = "/admin/reminders/"
  , fmElements =
    [ FormElement "message" "Reminder Message" InputText True False
    , FormElement "occurs" "Reminder Time" InputDateTime True False
    , FormElement "event" "Associated Event" InputSelect True False
    ]
  , fmSubmitText = "Create Reminder"
  , fmDisabled = False
  }


updateReminderFormMeta :: Sql.Key Reminder -> FormMeta
updateReminderFormMeta rId = newReminderFormMeta
  { fmMethod = PUT
  , fmTarget = "/admin/reminders/" <> tshow (fromSqlKey rId)
  , fmSubmitText = "Update Reminder"
  }
