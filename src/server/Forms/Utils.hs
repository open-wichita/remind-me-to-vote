module Forms.Utils where

import           Lucid.Bootstrap3
import           Forms.Bootstrap3

import           ClassyPrelude
import           Data.Char      (isDigit)
import           Data.Text      (strip)
import           Data.Time
import           Text.Digestive
import           Lucid


utcDateTimeFormlet :: Monad m => String -> Formlet Text m UTCTime
utcDateTimeFormlet fmt d =
  validate (validateTime fmt "invalid date") (string $ formatTime defaultTimeLocale fmt <$> d)


validateTime :: ParseTime a => String -> Text -> String -> Result Text a
validateTime fmt err x
  | length x < 40 = maybe (Error err) Success $ parseTimeM True defaultTimeLocale fmt x
  | otherwise = Error "Not a valid date/time string"


-- Extremely US-centric currently
phoneNumberFormlet :: Monad m => Formlet Text m Text
phoneNumberFormlet num =
  -- The minimum number of digits we are actually looking for is 10 (extracted
  -- form a format like XXX-XXX-XXXX), but `normalizeNumber` adds a `+1` to a
  -- number of only length 10, so actually require a min of 12 at that point
  validate (minMaxLen (12, 16) . normalizeNumber) (text num)


-- Get the E.164 format for the given number. Adds missing country code and area
-- code if necessary. Currently very simple and only works for certain US numbers.
-- TODO: migrate to phone-numbers for this stuff
normalizeNumber :: Text -> Text
normalizeNumber =
  (<>) "+" . addCountryCode . stripNumber
  where
    stripNumber = filter isDigit
    addCountryCode num = if length num <= 10
                            then "1" <> addAreaCode num
                            else num
    addAreaCode num = if length num <= 7
                         then "316" <> num
                         else num


minMaxLen :: (Int, Int) -> Text -> Result Text Text
minMaxLen (minLen, maxLen) t =
  if len >= minLen && len <= maxLen
    then Success stripped
    else Error $ "Must be longer than " <> tshow minLen <> " and shorter than " <> tshow maxLen <> " characters"
  where
    stripped = strip t
    len = length stripped


optionalChoice :: (Num a, Eq a, Monad m, Monoid v) => [(a, v)] -> Maybe a -> Form v m (Maybe a)
optionalChoice choices def = validate opt (choice choices def)
  where
    opt ch
      | ch == 0 = return Nothing
      | otherwise = return $ Just ch


-- see http://stackoverflow.com/a/9464847
modalForm :: Text -- ^ Button name
          -> FormMeta
          -> View (Html ())
          -> Html ()
modalForm name formMeta form = do
  -- TODO: better id names
  button_ [type_ "button", class_ "btn btn-primary", dataToggle_ "modal", dataTarget_ "#modalForm"] (toHtml name)
  div_ [class_ "modal fade", id_ "modalForm", tabindex_ "-1", role_ "dialog"] $
    div_ [class_ "modal-dialog", role_ "document"] $
      div_ [class_ "modal-content"] $ do
        div_ [class_ "modal-header"] $ do
          button_ [type_ "button", class_ "close", dataDismiss_ "modal", ariaLabel_ "Close"] $ span_ [ariaHidden_ True] htmlTimesSymbol
          h4_ [class_ "modal-title"] (toHtml name)
        div_ [class_ "modal-body"] $
          -- TODO: parameterize form render function?
          renderForm formMeta form
        -- div_ [class_ "modal-footer"] $ do
        --   button_ [type_ "button", class_ "btn btn-default", dataDismiss_ "modal"] "Close"
        --   -- TODO: hook up to form with the `form` attribute, need a render function that does not add submit button
        --   button_ [type_ "button", class_ "btn btn-primary"] "Save changes"
