-- An adaptation of digestive-bootstrap
module Forms.Bootstrap3
    ( FormMeta (..), FormElement (..), FormElementCfg (..)
    , StdMethod (..)
    , renderForm
    , renderFormSimple
    ) where

import           Control.Monad              (Monad ((>>), (>>=)), mapM_)
import           Data.Bool                  (Bool (..), (||))
import           Data.Eq                    (Eq ((/=), (==)))
import           Data.Function              (const, ($))
import           Data.Functor               (Functor (fmap))
import           Data.Int                   (Int)
import           Data.Maybe                 (Maybe (..))
import           Data.Monoid                (Monoid (mempty), (<>))
import           Data.Text                  (Text)
import           Data.Text.Encoding         (decodeUtf8)
import           Lucid
import           Lucid.Bootstrap3
import           Network.HTTP.Types.Method  (StdMethod (..), renderStdMethod)
import           Text.Digestive
import           Text.Digestive.Lucid.Html5


type NumberUnit = Text


-- | Form element type
data FormElementCfg
  = InputText
  | InputNumber (Maybe NumberUnit)
  | InputPassword
  | InputTextArea (Maybe Int) (Maybe Int)
  | InputHidden
  | InputSelect
  | InputRadio Bool
  | InputCheckbox
  | InputFile
  | InputDate
  | InputDateTime
  | InputEmail
  | InputTelephone


-- | Configuration for a form element
data FormElement
  = FormElement
  { feName     :: Text
  , feLabel    :: Text
  , feCfg      :: FormElementCfg
  , feRequired :: Bool
  , feDisabled :: Bool
  }


-- | Meta information for a HTML form
data FormMeta
  = FormMeta
  { fmMethod     :: StdMethod
  , fmTarget     :: Text
  , fmElements   :: [FormElement]
  , fmSubmitText :: Text
  , fmDisabled   :: Bool
  }


-- | Render a form defined by 'FormMeta' information and
-- the digestive functor 'View'.
renderForm :: FormMeta -> View (Html ()) -> Html ()
renderForm formMeta formView =
  form_ [role_ "form", method_ actualFormMethod, action_ formAction] $ do
    mapM_ (renderElement True formView) formElements
    formSubmit_ (toHtml $ fmSubmitText formMeta)
  where
    actualFormMethod =
      case fmMethod formMeta of
        POST -> methodPost
        PUT -> methodPost
        _ -> methodGet
    methodGet = decodeUtf8 $ renderStdMethod GET
    methodPost = decodeUtf8 $ renderStdMethod POST

    formAction = fmTarget formMeta <> methodOverridePart
    formMethod = decodeUtf8 $ renderStdMethod (fmMethod formMeta)
    methodOverridePart =
      if fmMethod formMeta /= GET || fmMethod formMeta /= POST
        then "?_method=" <> formMethod
        else ""

    formElements = if fmDisabled formMeta
                      then fmap (\e -> e { feDisabled = True }) (fmElements formMeta)
                      else fmElements formMeta


-- | Render a form defined by 'FormMeta' information and
-- the digestive functor 'View'.
renderFormSimple :: FormMeta -> View (Html ()) -> Html ()
renderFormSimple formMeta formView =
  form_ [role_ "form", method_ formMethod, action_ formAction, class_ "form-simple"] $ do
    mapM_ (renderElement False formView) (fmElements formMeta)
    formSubmit_ (toHtml $ fmSubmitText formMeta)
  where
    formMethod = decodeUtf8 $ renderStdMethod (fmMethod formMeta)
    formAction = fmTarget formMeta


renderElement :: Bool -> View (Html ()) -> FormElement -> Html ()
renderElement showLabel formView formElement =
  formGroup_ $ do
    case errors (feName formElement) formView of
      [] -> mempty
      errorMsgs ->
        alert_ BootAlertDanger $ ul_ $ mapM_ li_ errorMsgs
    if labelWrapInput
      then div_ [class_ labelWrapClass] $ label_ [class_ labelClass] $ element >> toHtml (feLabel formElement)
      else do
        label_ [for_ (feName formElement), class_ labelClass] $ toHtml (feLabel formElement)
        let ct = with element [class_ "form-control", placeholder_ (feLabel formElement)]
        if hasAddon
          then div_ [class_ "input-group"] (ct >>= const groupAddonAfter)
          else ct
  where
    (hasAddon, groupAddonAfter) =
      case feCfg formElement of
        InputNumber (Just numberUnit) ->
            (True, span_ [class_ "input-group-addon"] $ toHtml numberUnit)
        _ ->
            (False, mempty)
    (labelWrapInput, labelWrapClass) =
      case feCfg formElement of
        InputCheckbox -> (True, "checkbox")
        InputRadio _ -> (True, "radio")
        _ -> (False, "")
    labelClass =
      case (feCfg formElement, showLabel) of
        (InputCheckbox, False)  -> ""
        (_, False)  -> "sr-only"
        _ -> ""
    element = with
                (buildFun (feName formElement) formView)
                ( if feRequired formElement then [required_ ""] else mempty
               <> if feDisabled formElement then [disabled_ ""] else mempty
                )
    buildFun =
      case feCfg formElement of
        InputText -> inputText
        InputPassword -> inputPassword
        InputTextArea taRows taCols -> inputTextArea taRows taCols
        InputHidden -> inputHidden
        InputSelect -> inputSelect
        InputRadio rBr -> inputRadio rBr
        InputCheckbox -> inputCheckbox
        InputFile -> inputFile
        InputNumber _ -> inputX "number"
        InputDate -> inputX "date"
        InputDateTime -> inputX "datetime"
        InputEmail -> inputX "email"
        InputTelephone -> inputX "tel"


inputX :: Text -> Text -> View v -> Html ()
inputX x ref view =
  input_
    ([
      type_ x
    , id_ ref'
    , name_ ref'
    , value_ (fieldInputText ref view)
    ] <> if x == "number"
            then [step_ "any"]
            else mempty
    )
  where
    ref' = absoluteRef ref view
