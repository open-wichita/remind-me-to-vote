{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE TypeFamilies     #-}
module Utils where

import qualified Blaze.ByteString.Builder     as Blaze
import           ClassyPrelude
import           Control.Monad.Logger         (LoggingT, runStdoutLoggingT)
import           Control.Monad.Trans.Resource (ResourceT, runResourceT)
import           Database.Persist             (Key, PersistEntity,
                                               PersistEntityBackend, get)
import           Database.Persist.Sql         (SqlBackend, SqlPersistT,
                                               runSqlConn)
import           Lucid                        (Html, execHtmlT)
import           Network.HTTP.Types.Status    (status404)
import           Web.Spock.Shared             (ActionCtxT, HasSpock, SpockConn,
                                               bytes, html, runQuery, setHeader,
                                               setStatus)


lucid :: MonadIO m => Html () -> ActionCtxT ctx m a
lucid = html . decodeUtf8 . Blaze.toByteString . runIdentity . execHtmlT
{-# INLINE lucid #-}


runSql :: (HasSpock m, SpockConn m ~ SqlBackend) => SqlPersistT (LoggingT (ResourceT IO)) a -> m a
runSql action =
  runQuery $ \conn ->
    runResourceT $ runStdoutLoggingT $ runSqlConn action conn
{-# INLINE runSql #-}


xml :: MonadIO m => Text -> ActionCtxT ctx m a
xml val = do
  setHeader "Content-Type" "text/xml; charset=utf-8"
  bytes $ encodeUtf8 val
{-# INLINE xml #-}


get404
  :: forall b ctx (m :: * -> *).
     ( MonadIO m
     , HasSpock (ActionCtxT ctx m)
     , PersistEntity b
     , PersistEntityBackend b ~ SqlBackend
     , SpockConn (ActionCtxT ctx m) ~ SqlBackend
     )
  => Key b
  -> ActionCtxT ctx m b
get404 id' = do
  mEntity <- runSql $ get id'
  case mEntity of
    Just e -> return e
    Nothing -> do
      setStatus status404
      html "Not found"
