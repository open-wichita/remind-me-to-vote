module Twilio.Spock where

import           ClassyPrelude
import           Network.Wai.Extended (getRequestedUri)
import           Twilio.Extended      (AuthToken, verifyRequest)
import           Web.Spock            (ActionCtxT, header, params, request)


verifyRequestSpock :: MonadIO m => AuthToken -> ActionCtxT ctx m Bool
verifyRequestSpock authToken = do
  mSig <- header "X-Twilio-Signature"
  case mSig of
    Nothing -> return False
    Just sig -> do
      pars <- params
      req <- request
      let uri = getRequestedUri req
      return $ verifyRequest uri pars sig authToken
