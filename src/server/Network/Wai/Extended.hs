module Network.Wai.Extended
  ( module Network.Wai
  , getRequestedUri
  , getApproot
  ) where

import           ClassyPrelude
import qualified Data.Text                as T
import qualified Data.Text.Encoding       as TE
import qualified Data.Text.Encoding.Error as TEE
import           Network.Wai              (Request, pathInfo, requestHeaderHost)
import qualified Network.Wai.Request


getRequestedUri :: Request -> Text
getRequestedUri req = concat
  [ getApproot "" req
  , "/"
  , T.intercalate "/" $ pathInfo req
  ]


getApproot :: Text -> Request -> Text
getApproot fallback req =
  case requestHeaderHost req of
    Nothing -> fallback
    Just host ->
      (if Network.Wai.Request.appearsSecure req
          then "https://"
          else "http://")
      <> TE.decodeUtf8With TEE.lenientDecode host
