{-# LANGUAGE DataKinds #-}
module Routes where

import           Actions
import           Routes.Hooks
import           Types


import           ClassyPrelude hiding (delete)
import           Data.HVect    (HVect (..))
import           Web.Spock     (delete, get, getpost, post, prehook, put, root,
                                subcomponent, var, (<//>))


routes :: App (HVect '[])
routes = do
  get root rootAction

  prehook guestOnlyHook $ do
    getpost "register" registerAction
    getpost "login" loginAction

  prehook authHook $ do
    get "logout" logoutAction

    prehook adminHook $ subcomponent "admin" $ do
      get root adminRootAction

      getpost "events" adminEventsAction
      getput ("events" <//> var) adminSingleEventAction
      delete ("events" <//> var) adminDeleteSingleEventAction

      getpost "reminders" adminRemindersAction
      getput ("reminders" <//> var) adminSingleReminderAction
      delete ("reminders" <//> var) adminDeleteSingleReminderAction

      getpost "subscriptions" adminSubscriptionsAction
      getpost "subscriptions/numbers" adminSubscriptionNumbersAction
      getpost "subscriptions/emails" adminSubscriptionEmailsAction
      getput ("subscriptions" <//> var) adminSingleSubscriptionAction

  getpost "subscriptions/numbers" subscriptionNumberAction

  getpost "subscriptions/emails" subscriptionEmailAction

  post "receive_sms" receiveSmsAction
  where
    getput r a = get r a >> put r a
