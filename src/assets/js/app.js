$(function () {
    $('input[type=datetime]').datetimepicker({format: 'YYYY-MM-DDTHH:mm:SSZZ'});

    $('.btn-delete').click(function (e) {
        e.stopPropagation();
        e.preventDefault();
        var href = $(this).data('target');

        $('body').append('<div id="confirm" class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-body">Are you sure?</div><div class="modal-footer"><button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button> <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button></div></div></div></div>');

        $('#confirm')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#delete', function () {
                $('body').append('<form action="'+href+'" method="post" id="poster"></form>')
                $('#poster').submit();
            })
            .on('hidden.bs.modal', function () {
                $('#confirm').remove();
            })
            ;
    });
});
